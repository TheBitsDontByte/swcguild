var startAmount = 0;
var currAmount = 0;
var numOfRolls = 0;
var maxMoney = 0.00;
var numOfRollsAtMaxMoney = 0;
var readyToReset = false;
var playButton = document.querySelector('#playButton');
playButton.addEventListener('click', playGame);

function runAfterLoad() {
    getStartAmount();
}

function getStartAmount() {
    do {
        startAmount = prompt("How much money are you starting with?\nPlease enter whole dollar amounts");
        startAmount = Math.floor(startAmount);
    } while (startAmount <= 0 || !$.isNumeric(startAmount));        
    
    document.querySelector('#betForm').value = `$${startAmount}`;;   
}

function playGame () {
    
    if (readyToReset) { 
        resetGame();
        getStartAmount();
        return;
    }
    
    currAmount = startAmount;
    
    //Set the max values to pre-game defaults
    maxMoney = currAmount;
    numOfRollsAtMaxMoney = 0;
    
    //While there is money, keep playing
    while (currAmount > 0) {
        var dice1 = Math.floor(Math.random() * 7) + 1;
        var dice2 = Math.floor(Math.random() * 7) + 1;
        numOfRolls++;

        dice1 + dice2 === 7 ? currAmount +=4 : currAmount--;
        
        //update the max variables
        if (currAmount > maxMoney) {
            maxMoney = currAmount;
            numOfRollsAtMaxMoney = numOfRolls;
        }        
    }
    updateTable();
    readyToReset = true;
}

function resetGame() {
    document.getElementById('tableDiv').style.visibility = "hidden";
    //reset variables
    startAmount = 0;
    currAmount = 0;
    numOfRolls = 0;
    maxMoney = 0.00;
    numOfRollsAtMaxMoney = 0;
    readyToReset = false;
    document.getElementById('playButton').value = "Play";
}

//Set all the table values and then display, change the play button to play again
function updateTable() {
    document.getElementById('tableStartBet').textContent = `$${startAmount}`;
    document.getElementById('maxMoney').textContent = `$${maxMoney}`;
    document.getElementById('totalRolls').textContent = `${numOfRolls}`;
    document.getElementById('totalRollsAtMaxMoney').textContent = `${numOfRollsAtMaxMoney}`;
    document.getElementById('tableDiv').style.visibility = "visible";
    document.getElementById('playButton').value = "Play Again ?";
}