function validateItems() {
    //console.log("Hey");
    clearErrors();
    var num1 = document.getElementById('num1').value;
    var num2 = document.forms["mathStuff"]["num2"].value;
    if (num1 == "" || isNaN(num1)) {
        alert("Enter a valid number");
        document.forms["mathStuff"]["num1"]
            .parentElement.className += " has-error";
        document.forms["mathStuff"]["num1"].focus();
        return false;
    }
    if (num2 == "" || isNaN(num2)) {
        alert("Enter a valid number");
        document.forms["mathStuff"]["num2"]
            .parentElement.className += " has-error";
        document.forms["mathStuff"]["num2"].focus();
        return false;
    }
    num1 = parseInt(num1);
    num2 = parseInt(num2);
    var add = num1 + num2;
    var min = num1 - num2;
    var mult = num1 * num2;
    var div = num1 / num2;


    console.log(num1 + num2);
    document.getElementById('results').style.display = "block";
    document.getElementById('submitButton').innerText = "Recalculate";
    document.getElementById('addition').innerText =add;
    document.getElementById('subtraction').innerText =min;
    document.getElementById('multiplication').innerText =mult;
    document.getElementById('division').innerText =div;

    return false;
}

function clearErrors() {
    var formElements = document.forms["mathStuff"].elements;
    for (var i = 0; i < formElements.length;
            i++) 
        {
            if (formElements[i].parentElement
                    .className.indexOf("has-") != -1)
                {
                    formElements[i].parentElement.className = "form-group";
                }


        }
}

function resetForm() {
    clearErrors();
    document.getElementById('num1').innerHtml = "";
    document.forms["mathStuff"]["num1"].innerHtml = "";
    document.getElementById('submitButton').innerHTML = "Submit";
    document.getElementById('results').style.display = "none";
}