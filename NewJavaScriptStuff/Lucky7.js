var gameFinished = false;

function play() {
    if (gameFinished) {
        resetGame();
        gameFinished = false;
        return false;
    }
    var input = validateInput();
    if (input == -1)
        return false;
    var startAmount = input;
    var maxAmount = input;
    var maxRolls = 0;
    var roll = 0;
    
    while (input > 0) {
        roll++;
        var diceRoll = Math.floor((Math.random() * 7) + 1) + Math.floor((Math.random() * 7) + 1);
        if (diceRoll == 7)
            input += 4;
        else
            input--;
        if (input > maxAmount) {
            maxAmount = input;
            maxRolls = roll;
        }        
    }
    updateTable(startAmount, roll, maxAmount, maxRolls);
    return false;
}

function updateTable(input, roll, maxAmount, maxRolls) {
    document.getElementById('playButton').innerHTML = "Play Again ?"
    document.getElementById('startBet').innerText = input;
    document.getElementById('totalRolls').innerText = roll;
    document.getElementById('maxWon').innerText = maxAmount;
    document.getElementById('rollsAtHighest').innerText = maxRolls;
    document.getElementById('results').style.visibility = 'visible';
    gameFinished = true;
}

function resetGame() {
    document.getElementById('results').style.visibility = 'hidden';
    document.getElementById('betInput').value = "";
    document.getElementById('playButton').innerHTML = "Play"
}

function validateInput() {
    clearError();
    var input = 0;
    input = document.getElementById('betInput').value;
    if (isNaN(input) || input == "") {
        alert("Invalid Input");
        document.getElementById('betInput').parentElement.className
                += " has-error";
        document.getElementById('betInput').focus;
        return -1;        
    }
    return input;
}

function clearError() {
    document.getElementById('betInput').parentElement.className
                = "form-group";
}