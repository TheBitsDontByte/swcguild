var toggleOn = false;

function toggleTracking() {
 
    toggleOn = !toggleOn;
    if (toggleOn) 
        document.getElementById("trackingStatus").innerText = "TRACKING";
    else
        document.getElementById("trackingStatus").innerText = "NOT TRACKING";
    console.log(toggleOn);
}

function updateMousePosition() {
    if (toggleOn) {
        var mouseX = document.getElementById('mousePositionX');
        var mouseY = document.getElementById('mousePositionY');

        mouseX.innerText = event.clientX;
        mouseY.innerText = event.clientY;


    }
}